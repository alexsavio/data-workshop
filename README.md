# data-workshop

Pandas + ScikitLearn tutorial materials

# Interesting places

## Documentation

- [Pandas](http://pandas.pydata.org/pandas-docs/stable/)
- [Matplotlib](https://matplotlib.org/contents.html)
- [Scikit-Learn](https://scikit-learn.org/stable/documentation.html)


## Also interesting...

- [XArray](http://xarray.pydata.org/en/stable/)
- [Dataset](https://dataset.readthedocs.io/en/latest/)


- [Seaborn](https://seaborn.pydata.org/)
- [Altair](https://altair-viz.github.io/)
- [Bokeh](https://bokeh.pydata.org/en/latest/)


- [statsmodels](http://www.statsmodels.org/stable/index.html)
- [sklearn-pandas](https://github.com/scikit-learn-contrib/sklearn-pandas)
